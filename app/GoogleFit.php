<?php
/**
 * Created by PhpStorm.
 * User: SHANKS
 * Date: 7/27/2017
 * Time: 3:33 PM
 */

namespace App;


use Google_Client;
use Google_Service_Fitness;

class GoogleFit
{
    public function __construct()
    {

    }

    public function test(){
        $APIKey = 'AIzaSyCAK38qw7WvjqEEs7cBlGlSDffd0NvN0V8';
        $client_id = '293737649222-oadpg5ijdgn91281csp0sv24ehtj980j.apps.googleusercontent.com';
        $client_secret = 'U_KNtXze-jZTp-iNv7WBZS7h';
        //$redirect_uri = 'http://localhost/googlefit_test/get_user.php';
        $redirect_uri = 'http://localhost/fitbit/public/google';

////This template is nothing but some HTML. You can find it on github Google API example.
//include_once "templates/base.php";

//Start your session.
        session_start();

        $client = new Google_Client();
        $client->setApplicationName('google-fit');
        $client->setAccessType('online');
        $client->setApprovalPrompt("auto");
        $client->setClientId($client_id);
        $client->setClientSecret($client_secret);
        $client->setRedirectUri($redirect_uri);

        $client->addScope(Google_Service_Fitness::FITNESS_ACTIVITY_READ);
        $service = new Google_Service_Fitness($client);

        /************************************************
        If we're logging out we just need to clear our
        local access token in this case
         ************************************************/
        if (isset($_REQUEST['logout'])) {
            unset($_SESSION['access_token']);
        }
        /************************************************
        If we have a code back from the OAuth 2.0 flow,
        we need to exchange that with the authenticate()
        function. We store the resultant access token
        bundle in the session, and redirect to ourself.
         ************************************************/
        if (isset($_GET['code'])) {

            $client->authenticate($_GET['code']);
            $_SESSION['access_token'] = $client->getAccessToken();
            $redirect = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];
            $redirect = url('/').'/google';
            header('Location: ' . filter_var($redirect, FILTER_SANITIZE_URL));
            //echo "EXCHANGE";
        }
        if (isset($_SESSION['access_token']) && $_SESSION['access_token']) {
            $client->setAccessToken($_SESSION['access_token']);
//            echo "GOT IT";
//            echo 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];
//            echo "<pre>";


            $dataSources = $service->users_dataSources;
            $dataSets = $service->users_dataSources_datasets;

            $listDataSources = $dataSources->listUsersDataSources("me");

            $timezone = "UTC+5";
            $today = date("Y-m-d");
            $endTime = strtotime($today.' 22:00:00 '.$timezone.' -1 day');
            $startTime = strtotime('-1 day', $endTime);
            echo '<strong>Start Time: '.date('Y-m-d H:i:s',$startTime).'<br></strong>';
            echo '<strong>End Time: '.date('Y-m-d H:i:s',$endTime).'<br></strong>';
            echo $endTime.'<br>';


            while($listDataSources->valid()) {
                $dataSourceItem = $listDataSources->next();
//        print_r($dataSourceItem);
//        exit();

                /****   START com.google.step_count.delta    ****/
                if ($dataSourceItem['dataType']['name'] == "com.google.step_count.delta")
                {
                    $dataStreamId = $dataSourceItem['dataStreamId'];
//            $dataSourceId =  "derived:com.google.step_count.delta:com.google.android.gms:estimated_steps";
                    $listDatasets = $dataSets->get("me", $dataStreamId, $startTime.'000000000'.'-'.$endTime.'000000000');

                    $step_count = 0;
                    $total_steps = 0;

                    while($listDatasets->valid()) {
                        $dataSet = $listDatasets->next();
                //print_r($dataSet);
                        $startTimeNanos = $dataSet['startTimeNanos'];
                        $endTimeNanos = $dataSet['endTimeNanos'];
                        $dataSetValues = $dataSet['value'];
                        if ($dataSetValues && is_array($dataSetValues)) {
                            foreach($dataSetValues as $dataSetValue) {
                                echo 'Start Time: '.date('Y-m-d H:i:s',$startTimeNanos/1000000000).' -- End Time: '.date('Y-m-d H:i:s',$endTimeNanos/1000000000).'<br>'.$dataSetValue['intVal'].'<br>';
                                $step_count += $dataSetValue['intVal'];
                            }
                        }
                    }
                    print("<b>STEP: ".$step_count."<br /><br /><br /></b>");
//            exit();
                };

                /****   END com.google.step_count.delta    ****/


                /****   START com.google.step_count.cumulative    ****/
                $temp = [];
//        if($dataSourceItem['dataType']['name'] == "com.google.step_count.cumulative"){
//            $dataStreamId = $dataSourceItem['dataStreamId'];
//            $listDatasets = $dataSets->get("me", $dataStreamId, $startTime.'000000000'.'-'.$endTime.'000000000');
////            print_r($listDatasets);
//            $step_count = 0;
//            while($listDatasets->valid()) {
//                $dataSet = $listDatasets->next();
//                print_r($dataSet);
////                exit();
//                $dataSetValues = $dataSet['value'];
//                $start_nanos_time = $dataSet['startTimeNanos'];
//                $end_nanos_time = $dataSet['endTimeNanos'];
//                echo date('Y-m-d H:i:s',$start_nanos_time/1000000000 );
//                echo "<br>";
//                echo date('Y-m-d H:i:s',$end_nanos_time/1000000000 );
//                echo "<br>";
//                if ($dataSetValues && is_array($dataSetValues)) {
//                    foreach($dataSetValues as $dataSetValue) {
//                        echo $dataSetValue['intVal'].'<br>';
//
//                        $temp[$start_nanos_time.'-'.$dataStreamId] = $dataSetValue['intVal'];
//                        //$temp[$dataStreamId] += $dataSetValue['intVal'];
//                        $step_count += $dataSetValue['intVal'];
//                    }
//                }
//            }
//            print_r($temp);
//            print("<b>STEP: ".$step_count."<br /></b>");
//        }
                /****   END com.google.step_count.cumulative    ****/



                /****   START com.google.step_count.cadence    ****/
//        if($dataSourceItem['dataType']['name'] == "com.google.step_count.cadence"){
//            $dataStreamId = $dataSourceItem['dataStreamId'];
//            $listDatasets = $dataSets->get("me", $dataStreamId, $startTime.'000000000'.'-'.$startTime.'000000000');
//            print_r($listDatasets);
//            $step_count = 0;
//            while($listDatasets->valid()) {
//                $dataSet = $listDatasets->next();
////                print_r($dataSet);
////                exit();
//                $dataSetValues = $dataSet['value'];
//                if ($dataSetValues && is_array($dataSetValues)) {
//                    foreach($dataSetValues as $dataSetValue) {
//                        echo $dataSetValue['intVal'].'<br>';
//                        $step_count += $dataSetValue['intVal'];
//                    }
//                }
//            }
//            print("<b>STEP: ".$step_count."<br /></b>");
//        }
                /****   END com.google.step_count.cumulative    ****/



                /****   START com.google.activity.samples    ****/
//        if ($dataSourceItem['dataType']['name'] == "com.google.activity.segment")
//        {
//            $dataStreamId = $dataSourceItem['dataStreamId'];
//            $listDatasets = $dataSets->get("me", $dataStreamId, $startTime.'000000000'.'-'.$endTime.'000000000');
//            print_r($listDatasets);
//            $step_count = 0;
//            $total_steps = 0;
//            while($listDatasets->valid()) {
//                $dataSet = $listDatasets->next();
//
//
//                $startTimeNanos = $dataSet['startTimeNanos'];
//                $endTimeNanos = $dataSet['endTimeNanos'];
//                $dataSetValues = $dataSet['value'];
//                if ($dataSetValues && is_array($dataSetValues)) {
//                    foreach($dataSetValues as $dataSetValue) {
//                        echo 'Start Time: '.date('Y-m-d H:i:s',$startTimeNanos/1000000000).'  End Time: '.date('Y-m-d H:i:s',$endTimeNanos/1000000000).'<br>'.$dataSetValue['intVal'].'<br>';
//                        $step_count += $dataSetValue['intVal'];
//                    }
//                }
//            }
//            print("<b>STEP: ".$step_count."<br /><br /><br /></b>");
//        };
                /****   END com.google.activity.samples    ****/


            }
            echo "</pre>";
        } else {
            $authUrl = $client->createAuthUrl();
        }
//        if (isset($authUrl)) {
//            echo "<a class='login' href='" . $authUrl . "'>Connect Me!</a>";
//        }

    }
}