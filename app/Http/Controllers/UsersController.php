<?php

namespace App\Http\Controllers;

use App\GoogleFit;
use App\Http\Requests\RegisterRequest;
use App\User;

use App\FitbitClient;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;


class UsersController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function register(RegisterRequest $request){
        $validator = Validator::make(
            array(
                'first_name' => $_POST['first_name'],
                'last_name' => $_POST['last_name'],
                'email' => $_POST['email'],
                'password' => $_POST['password'],
                'password_confirmation' => $_POST['password_confirmation']
            ), array(
                'first_name' => 'required',
                'last_name' => 'required',
                // 'userName'  =>  'required | unique:users',
                'email' => 'required | email | unique:users',
                'password' => 'required | min:6 | confirmed',
                'password_confirmation' => 'required | min:6'
            )
        );

        if ($validator->fails()) {
            $messages = $validator->messages();
            return redirect('register')->with('message', $messages);
        } else {
            $user = new User;
            $user->first_name = $_POST['first_name'];
            $user->last_name = $_POST['last_name'];
            $user->email = $_POST['email'];
            $user->password = bcrypt($_POST['password']);
            $user->save();
            Auth::login($user);
            return Redirect::to('dashboard');

        }
    }
    public function dashboard(){
        return view('dashboard');
    }

    public function login($remember = 1){
        if (Auth::attempt(['password' => $_POST['password'], 'email' => $_POST['email']], $remember)) {
            return Redirect::to('/dashboard');
        } else {
            $error_message = 'Invalid Email or Password';
            Session::flash('error', $error_message);
            return Redirect::to(URL::previous());

        }
    }

    public function test(){
        session_start();
        if ((!isset($_SESSION['authFitbit']) || $_SESSION['authFitbit'] != 1) && isset($_REQUEST['code'])) {
            // Callback
        //dd('call back');
            $fclient = new FitbitClient($_REQUEST['code']);
            $_SESSION['parameters'] = $fclient->getParameters();
            $_SESSION['authFitbit']
                = 1;
            //return Redirect::to('dashboard');
        }

        if ((!isset($_SESSION['authFitbit']) || $_SESSION['authFitbit'] != 1)) {
            // First connection
            //dd('asdasdasd');
            dd(FitbitClient::getAuthorizationCode());
        }
        echo '<pre>';
        return Redirect::to('dashboard');
//        if (isset($_SESSION['authFitbit']) && $_SESSION['authFitbit'] == 1) {
//            //dd('standard');
//            // Standard mode
//            $fclient = new FitbitClient();
//            $fclient->setParameters($_SESSION['parameters']);
////            var_dump($_SESSION['parameters']);
////            exit();
//            print_r($fclient->getUserProfile());
//            echo '<br />';
////   print_r($fclient->getHeartRateIntraday());
//
//            echo '<br />';
////   print_r($fclient->getPublicActivities());
//
//            /*
//             * Get Daily Activity Summary
//             */
//            $today = date("Y-m-d");
//            //echo '<br />';
////   print_r($fclient->getActivitiesByDate($today));
//
//
//            /*
//             * Get Lifetime Stats
//             */
//            //echo '<br />';
////   print_r($fclient->getLifeTimeStatus());
//
//
//
//            /*
//             * Get Frequent Activities
//             */
//            //echo '<br />';
//            //print_r($fclient->getFrequentActivities());
//
//
//            /*
//             * Get Recent Activity Types
//             */
//            //echo '<br />';
////   print_r($fclient->getRecentActivityType());
//        }
    }
    public function getUserProfile(){
        session_start();
        $fclient = new FitbitClient();
        $fclient->setParameters($_SESSION['parameters']);
//            var_dump($_SESSION['parameters']);
//            exit();
        //print_r($fclient->getUserProfile());
        $profile = $fclient->getUserProfile();
        return view('profile' , ['profile' => $profile]);
    }
    public function getStatus(){
        session_start();
        $fclient = new FitbitClient();
        $fclient->setParameters($_SESSION['parameters']);
        $today = date("Y-m-d");
       $status = $fclient->getLifeTimeStatus();
        return view('status' , ['status' => $status]);
    }
    public function getRecentActivities(){
        session_start();
        $fclient = new FitbitClient();
        $fclient->setParameters($_SESSION['parameters']);
       $recent_activities = $fclient->getRecentActivityType();
        return view('recent_activity' , ['recent_activities' => $recent_activities]);
    }

    public function getFrequentActivities(){
        session_start();
        $fclient = new FitbitClient();
        $fclient->setParameters($_SESSION['parameters']);
        $frequent_activities = $fclient->getFrequentActivities();
        return view('frequent_activity' , ['frequent_activities' => $frequent_activities]);
    }

    public function getActivities(){
        session_start();
        $date = $_POST['date'];
        $fclient = new FitbitClient();
        $fclient->setParameters($_SESSION['parameters']);
        $today = date("Y-m-d");
        $activities = $fclient->getActivitiesByDate($date);
        return view('activities', ['activities' => $activities]);
    }

    public function google(){
        $google = new GoogleFit();
        $google->test();
    }

}
