<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Redirect;

Route::get('/', function () {
    if(\Illuminate\Support\Facades\Auth::check()){
        return Redirect::to('dashboard');
    }
    return Redirect::to('login');
});

//Route::get('/login', function (){
//    return view('login');
//});

Route::get('/login' ,['as' => 'login', function (){
    return view('login');
}]);

Route::post('/login', 'UsersController@login');

Route::get('/register', function (){
    return view('register');
});
Route::post('/register', 'UsersController@register');

Route::group(['middleware' => 'auth'], function () {

    Route::get('/logout', function() {
        \Illuminate\Support\Facades\Auth::logout();
        @session_start();
        //session_unset();
        session_destroy();
        if (isset($_SERVER['HTTP_COOKIE'])) {
            $cookies = explode(';', $_SERVER['HTTP_COOKIE']);
            foreach($cookies as $cookie) {
                $parts = explode('=', $cookie);
                $name = trim($parts[0]);
                setcookie($name, '', time()-1000);
                setcookie($name, '', time()-1000, '/');
            }
        }
        //dd($cookies);
        return Redirect::to('login');
    });

    Route::get('/dashboard', [
        'uses' =>   'UsersController@dashboard',
        'as' => 'home'
    ]);


Route::get('/test', 'UsersController@test');

Route::get('/user/profile', 'UsersController@getUserProfile');

Route::get('/status', 'UsersController@getStatus');

Route::get('/recent_activity', 'UsersController@getRecentActivities');

Route::get('/frequent_activity', 'UsersController@getFrequentActivities');

Route::post('/activity', 'UsersController@getActivities');
});

Route::get('/google', 'UsersController@google');




