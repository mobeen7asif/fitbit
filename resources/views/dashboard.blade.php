<!DOCTYPE html>
<html lang="en">
<?php include resource_path('views/includes/head.php'); ?>
    <body>

    <?php include resource_path('views/includes/header.php'); ?>
    <?php include resource_path('views/includes/sidebar.php'); ?>
    {{--{{session_start()}}--}}
        {{--{{dd(isset($_SESSION['parameters']))}};--}}
        <section class="content homeContent">
            <div class="contentPd">
                <div class="dashBoard">
                    <ul>
                        @if(!isset($_SESSION['authFitbit']))
                            <li>
                                <a href="{{url('/')}}/test">
                                    <img src="{{url('/')}}/images/fitbit.png" alt="">
                                    Fit Bit
                                </a>
                            </li>


                        @endif

                        <li>
                            <a href="#">
                                <img src="{{url('/')}}/images/googleFit.png" alt="">
                                Google Fit
                            </a>
                        </li>
                        
                    </ul>
                </div>
            </div>
        </section>
        <script src="public/js/jquery.min.js"></script>
        <script src="public/js/bootstrap.min.js"></script>
        <script src="public/js/jquery.dataTables.js"></script>
        <script src="public/js/mian.js"></script>

    </body>
</html>
