<head>
        <meta charset="UTF-8">
        <title>Fit Bit</title>
        <meta name="viewport" content="public/width=device-width">
        <link rel="stylesheet" href="<?php echo asset('css/bootstrap.min.css'); ?>">
        <link rel="stylesheet" href="<?php echo asset('css/font-awesome.min.css'); ?>">
        <link rel="stylesheet" href="<?php echo asset('css/jquery.dataTables.css'); ?>">
        <link rel="stylesheet" href="<?php echo asset('css/normalize.css'); ?>">
        <link rel="stylesheet" href="<?php echo asset('css/theme.css'); ?>">
        <link rel="stylesheet" type="text/css" href="<?php echo asset('css/jquery.datetimepicker.css'); ?>"/>
</head>