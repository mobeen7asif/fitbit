<aside class="sideBar">
    <div class="userPro">
        <figure>
            <img src="<?php echo asset('images/img.jpg'); ?>" alt="">
        </figure>
        <h4><?php echo \Illuminate\Support\Facades\Auth::user()->first_name.' '.\Illuminate\Support\Facades\Auth::user()->last_name ;?></h4>
    </div>
    <ul class="navList">
<?php
//session_start();
if(!isset($_SESSION))
{
    session_start();
}
//session_start();

//die(isset($_SESSION['parameters'])); ?>

        <?php if(isset($_SESSION['authFitbit'])) {
            ?>

        <li><a href="<?php echo asset('/status'); ?>">Life Time Status</a></li>


        <form method="post" action="<?php echo asset('/activity'); ?>">
            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
            <input type="hidden" name="date" value="<?php echo date('Y-m-d'); ?>">
            <input type="submit" value="User Activities">
        </form>
        <li><a href="<?php echo asset('/frequent_activity') ?>">Frequent Activities</a></li>
        <li><a href="<?php echo asset('/recent_activity') ?>">Recent Activities</a></li>
        <li><a href="<?php echo asset('/user/profile'); ?>">Profile</a></li>


        <?php } ?>
        <li><a href="<?php echo asset('/logout'); ?>">Logout</a></li>
    </ul>
</aside>

