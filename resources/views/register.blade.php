<html lang="en">
<?php include resource_path('views/includes/head.php'); ?>
<body>
<?php include resource_path('views/includes/header.php'); ?>
<section class="content publicContent loginPage">
    <div class="contentPd">
        <h2 class="mainHEading">Register</h2>
        <div class="userForm">
            <form action="{{url('/')}}/register" method="post" id="signupform">
                {{csrf_field()}}
                <label>
                    <span>First Name</span>
                    <input type="text" name="first_name" value="">
                </label>
                <label>
                    <span>Last Name</span>
                    <input type="text" name="last_name" value="">
                </label>
                <label>
                    <span>Password</span>
                    <input type=password name="password"  value="" id="password">
                </label>
                <label>
                    <span>Confrim Password</span>
                    <input type=password name="password_confirmation"  value="" id="confrim_password">
                </label>
                <label>
                    <span>Email</span>
                    <input type=text name="email" value="" id="email">
                </label>
                <div class="btnCol">
                    <input type="submit" name="signIn" value="SignUp" id="submitbtn">
                    <input type="reset" name="reset" value="Reset">
                </div>
            </form>
        </div>
    </div>
</section>
<script src="{{url('/')}}/js/jquery.min.js"></script>
<script src="{{url('/')}}/js/bootstrap.min.js"></script>
<script src="{{url('/')}}/js/jquery.dataTables.js"></script>
<script src="{{url('/')}}/js/mian.js"></script>
<script src="http://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>
<script src="http://jqueryvalidation.org/files/dist/additional-methods.min.js"></script>
<script>
    jQuery.validator.addMethod("alpha", function (value, element) {
        return this.optional(element) || /^[a-zA-Z ]*$/.test(value);
    }, "only letters,.");
    jQuery.validator.addMethod("alphanumeric", function (value, element) {
        return this.optional(element) || /([0-9].*[a-z])|([a-z].*[0-9])/.test(value);
    }, "password must contain only letters, numbers,.");
    $("#signupform").validate({
        rules:{
            first_name: {
                required: true,
                maxlength: 22,
                minlength: 3,
                alpha: true,
            },
            last_name:{
                required: true,
                minlength: 3,
                maxlength: 22,
                alpha: true,
            },
            password:{
                required: true,
                minlength: 5,
                maxlength: 22,
                alphanumeric:true,
            },
            confrim_password:{
                required: true,
                equalTo : "#password",
            },
            email:{
                required: true,
                email:    true,
            }
        },
        message:{
            alpha: "letters only"
        }
    });

</script>
</Body>
</html>
