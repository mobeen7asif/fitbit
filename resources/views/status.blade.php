<!DOCTYPE html>
<html lang="en">
<?php include resource_path('views/includes/head.php'); ?>
<body>

<?php include resource_path('views/includes/header.php'); ?>
<?php include resource_path('views/includes/sidebar.php'); ?>

<section class="content lifeContent">
    {{--{{dd($status)}}--}}
    <div class="contentPd">
        <h2 class="mainHEading">Life Time Activity</h2>
        <div class="lifeWidget">
            <label>
                <span>Active Score</span>
                <b><b>{{$status['lifetime']['total']['activeScore']}}</b></b>
            </label>
            <label>
                <span>Calories Out</span>
                <b>{{$status['lifetime']['total']['caloriesOut']}}</b>
            </label>
            <label>
                <span>Distances</span>
                <b>{{$status['best']['total']['distance']['value']}}</b>
            </label>
            <label>
                <span>Steps</span>
                <b>{{$status['lifetime']['total']['steps']}}</b>
            </label>
        </div>

    </div>
</section>
<script src="{{url('/')}}/js/jquery.min.js"></script>
<script src="{{url('/')}}/js/bootstrap.min.js"></script>
<script src="{{url('/')}}/js/jquery.dataTables.js"></script>
<script src="{{url('/')}}/js/mian.js"></script>

</body>
</html>