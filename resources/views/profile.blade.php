<!DOCTYPE html>
<html lang="en">
<?php include resource_path('views/includes/head.php'); ?>
    <body>
    <?php include resource_path('views/includes/header.php'); ?>
    <?php include resource_path('views/includes/sidebar.php'); ?>

        <section class="content homeContent">
            {{--{{dd($profile['user'])}}--}}
            <div class="contentPd">
                <h2 class="mainHEading">Profile</h2>
                <div class="profileWidget">
                    <label>
                        <span>First Name</span>
                        <b>{{$profile['user']['firstName']}}</b>
                    </label>
                    <label>
                        <span>Last Name</span>
                        <b>{{$profile['user']['lastName']}}</b>
                    </label>
                    <label>
                        <span>Member Since</span>
                        <b>{{$profile['user']['memberSince']}}</b>
                    </label>
                    <label>
                        <span>Encoded Id</span>
                        <b>{{$profile['user']['encodedId']}}</b>
                    </label>
                    <label>
                        <span>Age</span>
                        <b>{{$profile['user']['age']}}</b>
                    </label>
                    <label>
                        <span>Date Of Birth</span>
                        <b>{{$profile['user']['dateOfBirth']}}</b>
                    </label>
                    <label>
                        <span>Foods Locale</span>
                        <b>{{$profile['user']['foodsLocale']}}</b>
                    </label>
                    <label>
                        <span>Weight</span>
                        <b>{{$profile['user']['weight']}}</b>
                    </label>
                </div>
            </div>
        </section>
        <script src="{{url('/')}}/js/jquery.min.js"></script>
        <script src="{{url('/')}}/js/bootstrap.min.js"></script>
        <script src="{{url('/')}}/js/jquery.dataTables.js"></script>
        <script src="{{url('/')}}/js/mian.js"></script>

    </body>
</html>
