<html lang="en">
<?php include resource_path('views/includes/head.php'); ?>
<body>
<?php include resource_path('views/includes/header.php'); ?>
<section class="content publicContent loginPage">
    <div class="contentPd">
        <div class="userForm">
            <div class="alert alert-danger fade in alert-dismissable">
                <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                <strong>Error!</strong> Please Enter Valid Email and Password.
            </div>
            <form action="{{url('/')}}/login" method="post" id="loginform">
                {{csrf_field()}}
                <label class="fullField">
                    <span>Email</span>
                    <input type="text" name="email" value="">
                </label>
                <label class="fullField">
                    <span>Password</span>
                    <input type=password name="password" value="">
                </label>
                <div class="btnCol">
                    <input type="submit" name="signIn"  value="Login">
                </div>
            </form>
            <a href="{{url('/')}}/register">Sign Up</a>
        </div>
    </div>
</section>
<script src="{{url('/')}}/js/jquery.min.js"></script>
<script src="{{url('/')}}/js/bootstrap.min.js"></script>
<script src="{{url('/')}}/js/jquery.dataTables.js"></script>
<script src="{{url('/')}}/js/mian.js"></script>
<script src="http://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>
<script src="http://jqueryvalidation.org/files/dist/additional-methods.min.js"></script>
<script>
    $(document).ready(function () {
        $("#loginform").validate({
            rules:{
                password:{
                    required: true,
                },

                email:{
                    required: true,
                    email:    true,
                },
            },
            messages:{
                password:{
                    required:  'password is required',
                },
                email:{
                    required: 'email is required',
                }
            },
        });
    });
</script>
</Body>
</html>
