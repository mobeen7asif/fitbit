<!DOCTYPE html>
<html lang="en">
<?php include resource_path('views/includes/head.php'); ?>
<body>

<?php include resource_path('views/includes/header.php'); ?>
<?php include resource_path('views/includes/sidebar.php'); ?>
        <section class="content lifeContent">
            <div class="contentPd">
                {{--{{dd($recent_activities)}}--}}
                <h2 class="mainHEading">Recent Activity</h2>
                <table id="tableStyle" class="display" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th>Calories</th>
                        <th>Description</th>
                        <th>Distance</th>
                        <th>Duration</th>
                        <th>Name</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($frequent_activities as $activity)
                        <tr>
                            <td>{{$activity['calories']}}</td>
                            <td>{{$activity['description']}}</td>
                            <td>{{$activity['distance']}}</td>
                            <td>{{$activity['duration']}}</td>
                            <td>{{$activity['name']}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </section>
<script src="{{url('/')}}/js/jquery.min.js"></script>
<script src="{{url('/')}}/js/bootstrap.min.js"></script>
<script src="{{url('/')}}/js/jquery.dataTables.js"></script>
<script src="{{url('/')}}/js/mian.js"></script>

<script>
    $(document).ready(function () {
        $('#tableStyle').DataTable({
            columnDefs: [{
                targets: [0],
                orderData: [0, 1]
            }, {
                targets: [1],
                orderData: [1, 0]
            }, {
                targets: [1],
                orderData: [1, 0]
            }],
            order: [[0, false]],
            bSort: false
        });
    });
</script>

    </body>
</html>