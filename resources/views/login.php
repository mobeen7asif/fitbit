<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require 'db_connection.php';


session_start();
//session_unset();
if (isset($_POST['signIn'])) {


$email = $_POST['email'];
$password = $_POST['password'];

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $user_name = mysqli_real_escape_string($con, $_POST['email']);
        $password = md5(mysqli_real_escape_string($con, $_POST['password']));
        $sql = "SELECT * FROM users WHERE email = '$email' AND password = '$password'";
        $result = mysqli_query($con, $sql);
        $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
        $count = mysqli_num_rows($result);
        
        // If result matched $myusername and $mypassword, table row must be 1 row
        if($count == 1) 
        {
            
            $_SESSION['id'] = $row['id'];
            $_SESSION['email'] = $row['email'];
            
            header("location: dashboard.blade.php");
        }
        else 
        {
          
            echo  "Your Login Name or Password is invalid";
         }
        
    }
}
?>

<html lang="en">
    <?php include 'includes/head.php'; ?>
    <body>
        <?php include 'includes/header.php'; ?>
        <section class="content publicContent loginPage">
            <div class="contentPd">
                <div class="userForm">
                    <div class="alert alert-danger fade in alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                        <strong>Error!</strong> Please Enter Valid Email and Password.
                    </div>
                    <form action="login.php" method="post" id="loginform">
                        <label class="fullField">
                            <span>Email</span>
                            <input type="text" name="email" value="">
                        </label>
                        <label class="fullField">
                            <span>Password</span>
                            <input type=password name="password" value="">
                        </label>
                        <div class="btnCol">
                            <input type="submit" name="signIn"  value="Login">
                        </div>
                    </form>
                </div>
            </div>
        </section>
        <script src="public/js/jquery.min.js"></script>
        <script src="public/js/bootstrap.min.js"></script>
        <script src="public/js/jquery.dataTables.js"></script>
        <script src="public/js/mian.js"></script>
        <script src="http://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>
        <script src="http://jqueryvalidation.org/files/dist/additional-methods.min.js"></script>
        <script>
            $(document).ready(function () {
              $("#loginform").validate({
                    rules:{
                        password:{
                        required: true,
                    },
                    
                        email:{
                        required: true,
                        email:    true,
                    },
                },
                    messages:{
                        password:{
                           required:  'password is required',
                        },
                        email:{
                            required: 'email is required',
                        }
                    },
            });
        });
        </script>
    </Body>
</html>