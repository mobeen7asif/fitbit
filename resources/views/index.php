<?php
require 'db_connection.php';
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

if ($_POST) {


    $first_name = $_POST['first_name'];
    $last_name = $_POST['last_name'];
    $email = "";
    $password = "";
    $confrim_password = "";


    if (!isset($_POST['password']) || trim($_POST['password']) == '') {
        echo "please enter password.<br>";
    } else {
        $password = md5($_POST['password']);
    }
    if (!isset($_POST['confrim_password']) || trim($_POST['confrim_password']) == '') {
        echo "please enter password.<br>";
    } else {
        $confrim_password = md5($_POST['confrim_password']);
    }

    if (!isset($_POST['email']) || trim($_POST['email']) == '') {
        echo "please enter email.<br>";
    } else {
        $email = $_POST['email'];
    }

    if ($password != "" || $email != "") {

        if ($con->connect_error) {
            die("Connection failed: " . $con->connect_error);
        }
        $sql = "INSERT INTO users (first_name, last_name,password,confrim_password,email)
            VALUES ('$first_name', '$last_name','$password','$confrim_password','$email')";
        if ($con->query($sql) === TRUE) {

            $last_id = $con->insert_id;
            echo $last_id;

            header("location: dashboard.blade.php");
            echo "New record created successfully";
        } else {
            echo "Error: " . $sql . "<br>" . $con->error;
        }
        $con->close();
    }
}
?>
<html lang="en">
<?php include 'includes/head.php'; ?>
    <body>
<?php include 'includes/header.php'; ?>
        <section class="content publicContent loginPage">
            <div class="contentPd">
                <h2 class="mainHEading">Register</h2>
                <div class="userForm">
                    <form action="index.php" method="post" id="signupform">
                        <label>
                            <span>First Name</span>
                            <input type="text" name="first_name" value="">
                        </label>
                        <label>
                            <span>Last Name</span>
                            <input type="text" name="last_name" value="">
                        </label>
                        <label>
                            <span>Password</span>
                            <input type=password name="password"  value="" id="password">
                        </label>
                        <label>
                            <span>Confrim Password</span>
                            <input type=password name="confrim_password"  value="" id="confrim_password">
                        </label>
                        <label>
                            <span>Email</span>
                            <input type=text name="email" value="" id="email">
                        </label>
                        <div class="btnCol">
                            <input type="submit" name="signIn" value="SignUp" id="submitbtn">
                            <input type="reset" name="reset" value="Reset">
                        </div>
                    </form>
                </div>
            </div>
        </section>
        <script src="public/js/jquery.min.js"></script>
        <script src="public/js/bootstrap.min.js"></script>
        <script src="public/js/jquery.dataTables.js"></script>
        <script src="public/js/mian.js"></script>
        <script src="http://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>
        <script src="http://jqueryvalidation.org/files/dist/additional-methods.min.js"></script>
        <script>

            jQuery.validator.addMethod("alpha", function (value, element) {
                return this.optional(element) || /^[a-zA-Z ]*$/.test(value);
            }, "only letters,.");
            jQuery.validator.addMethod("alphanumeric", function (value, element) {
                return this.optional(element) || /([0-9].*[a-z])|([a-z].*[0-9])/.test(value);
            }, "password must contain only letters, numbers,.");
            $("#signupform").validate({
                rules:{
                    first_name: {
                        required: true,
                        maxlength: 22,
                        minlength: 3,
                        alpha: true,
                    },
                    last_name:{
                        required: true,
                        minlength: 3,
                        maxlength: 22,
                        alpha: true,
                    },
                    password:{
                        required: true,
                        minlength: 5,
                        maxlength: 22,
                        alphanumeric:true,
                    },
                    confrim_password:{
                        required: true,
                        equalTo : "#password",
                    },
                    email:{
                        required: true,
                        email:    true,
                    }
                },
                message:{
                    alpha: "letters only"
                }
            });
            
        </script>
    </Body>
</html>
