$(document).ready(function() {	
	$('ul li:nth-child(5n)').addClass('fifth-child');
	$('ul li:nth-child(4n)').addClass('fourth-child');
	$('ul li:nth-child(3n)').addClass('third-item');
	$('ul li:first-child').addClass('first-item');
	$('ul li:last-child').addClass('last-item');
	$('.wrapper .box01:last-child,.wrapper .box02:last-child').addClass('last-item');
	$('ul li').addClass('odd-item');
	$('ul li:nth-child(2n)').removeClass('odd-item').addClass('even-item');
	$('.simple-list02 li:nth-child(4n)').addClass('fourth-child');
	
	$(".mobile-btn").click(function(){
        $(".main-nav").slideToggle();
		var hasclass = $("#navIcon").hasClass('fa-navicon');
		if(hasclass){
			$("#navIcon").removeClass('fa-navicon').addClass('fa-close');
		}
		else {
			$("#navIcon").addClass('fa-navicon').removeClass('fa-close');
		}
    });
});


$(window).on('scroll',function() {    
    var scroll = $(window).scrollTop();

    if (scroll >= 300) {
        $(".header").addClass("stikyHead");
    } else {
        $(".header").removeClass("stikyHead");
    }
});
